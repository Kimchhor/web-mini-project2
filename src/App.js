import React , {useState} from 'react';
import AddInformation from './Components/AddInformation';
import TableData from './Components/TableData';

function App () {
  const [addName,setAddName]= useState([]);
  const [addEmail, setAddEmail] =useState([]);
  const [data, setData] = useState([
    {
      id: 1,
      name: "Seth",
      gender: 'Male',
      email:"piseth99@gmail.com",
      job:"Student",
      creAt:"27/05/2021",
      upAt:"10mn",
    },
    {
      id: 2,
      name: "Pich",
      gender: 'Male',
      email:"pich99@gmail.com",
      job:"Student",
      creAt:"27/05/2021",
      upAt:"10mn",
    },
  ]);
  var today = new Date(),
  date = today.getDate()+ '/' + (today.getMonth() + 1) + '/' +  today.getFullYear() ;
  const onAddData= (addName,addEmail,addPosition,addGender )=>{
    let addNew = {
      id:data.length+1,
      name: addName,
      email:addEmail,
      job:addPosition,
      gender:addGender,
      creAt:date,
    

    }
    setData([...data,addNew])
  }
  const [display,setDisplay] = useState('')
  const addDisplay = (type)=>{
    setDisplay(type)
  }
  const deleteRow = async (index, e) => {
    var newData = data;
    newData.splice(index, 1);

    setData([...newData]);
  };
    return (
      <div>
        <AddInformation onAddData={onAddData} addDisplay={addDisplay} display={display}/>
        <TableData data={data} deleteRow={deleteRow}/>
      </div>
    );
}

export default App;
