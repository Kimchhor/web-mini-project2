import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Form, Row, Col, Button } from 'react-bootstrap';

function AddInformation({ onAddData, addDisplay, display }) {
    const [addName, setAddName] = useState('');
    const [addEmail, setAddEmail] = useState('');
    const [addGender, setAddGender] = useState('');
    const [addPosition, setAddPosition] = useState('')
    function handlePosition(e) {
        const target = e.target;
        var value = target.value;
        setAddPosition(value)

    }
    function handleGender(e) {
        const target = e.target;
        var value = target.value;
        setAddGender(value)

    }
    let errors = {}
    if (!addName.trim()) {
        errors.addName = "Username is required"
    }
    if (!addEmail) {

        errors.addEmail = "Email is required"
    } else if (!/^\S+@\S+\.[a-z]{3}$/g.test(addEmail)) {
        errors.addEmail = "Email is invalid"
    }
    return (

        <Container>
            <Row>
                <Col>
                    <Form>
                        <h1>Personal Information</h1>
                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Username</Form.Label>
                            <Form.Control type="text" placeholder="Username" value={addName} onChange={(e) => setAddName(e.target.value)} />
                            {errors.addName && <p style={{ color: 'red' }}>{errors.addName}</p>}
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formBasicPassword">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Email" value={addEmail} onChange={(e) => setAddEmail(e.target.value)} />
                            {errors.addEmail && <p style={{ color: 'red' }}>{errors.addEmail}</p>}
                        </Form.Group>
                        <div className="text-right">
                            <Button className="m-1" variant="primary" onClick={() => onAddData(addName, addEmail, addPosition, addGender)}>Submit</Button>
                            <Button variant="secondary">Cancel</Button>
                        </div>

                    </Form>
                </Col>
                <Col>
                    <br />
                    <br />
                    <br />
                    <Form>
                        <h3>Gender</h3>
                        {['radio'].map((type) => (
                            <div key={`inline-${type}`} className="mb-3">
                                <Form.Check
                                    inline
                                    label="Male"
                                    name="group1"
                                    type={type}
                                    value="Male"
                                    id={`inline-${type}-1`}
                                    onChange={handleGender}
                                />
                                <Form.Check
                                    inline
                                    label="Female"
                                    name="group1"
                                    type={type}
                                    value="Female"
                                    id={`inline-${type}-2`}
                                    onChange={handleGender}
                                />
                            </div>
                        ))}
                        <h3>Job</h3>
                    </Form>
                    <Form>
                        {['checkbox'].map((type) => (
                            <div key={`inline-${type}`} className="mb-3">
                                <Form.Check
                                    inline
                                    label="Student"
                                    name="addPosition"
                                    type={type}
                                    id={`inline-${type}-1`}
                                    value="Student"
                                    onChange={handlePosition}
                                />
                                <Form.Check
                                    inline
                                    label="Teacher"
                                    name="addPosition"
                                    type={type}
                                    id={`inline-${type}-2`}
                                    value="Teacher"
                                    onChange={handlePosition}
                                />
                                <Form.Check
                                    inline
                                    label="Developer"
                                    name="addPosition"
                                    type={type}
                                    id={`inline-${type}-3`}
                                    value="Developer"
                                    onChange={handlePosition}
                                />
                            </div>
                        ))}
                    </Form>
                </Col>
            </Row>
        </Container>
    )
}

export default AddInformation


