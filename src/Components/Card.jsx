import React from 'react'
import { Card, Button, Dropdown } from 'react-bootstrap'
import { BsEye, BsTrash, BsArrowRepeat, BsCardList, BsGrid } from "react-icons/bs";

const CardHead = {
    backgroundColor: '#cabfbf',
    textAlign: 'center',
    padding: '5px'
}

function Cards({ data, deleteRow, index }) {
    return (
        <>
            <Card>
                <div style={CardHead}>
                    <Dropdown>
                        <Dropdown.Toggle variant="success" id="dropdown-basic">Action</Dropdown.Toggle>
                        <Dropdown.Menu style={{ background: "azure", textAlign: "center" }}>
                            <Dropdown.Item href="#/action-1">
                                <Button variant="primary"><BsEye /> View</Button>
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-2">
                                <Button variant="info"><BsArrowRepeat /> Update</Button>
                            </Dropdown.Item>
                            <Dropdown.Item href="#/action-3">
                                <Button variant="danger" onClick={() => deleteRow(index)}><BsTrash /> Delete</Button>
                            </Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
                <div style={{ padding: '10px' }}>
                    <h5>{data.name}</h5>
                    <h6>Job</h6>
                    <p className="ml-4">{data.job}</p>
                </div>
                <div style={CardHead}>
                    <p>{data.upAt}</p>
                </div>
            </Card>
        </>
    )
}

export default Cards
