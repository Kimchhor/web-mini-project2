import { render } from '@testing-library/react'
import React from 'react'
import { Container, Button, Table, Row, Col } from 'react-bootstrap'
import Cards from "../Components/Card"
import Detail from '../page/Detail'
import { BsEye,BsTrash,BsArrowRepeat,BsCardList,BsGrid } from "react-icons/bs";

function TableData({ data, deleteRow, index }) {
    return (
        <Container>
            <h1>Display Data As :</h1>
            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                <li class="nav-item mr-1" role="presentation">
                    <Button class="nav-link active btn btn-primary" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><BsCardList/> Table</Button>
                </li>
                <li class="nav-item" role="presentation">
                    <Button class="nav-link btn btn-secondary" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><BsGrid/> Cards</Button>
                </li>
            </ul>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <Table striped bordered hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Email</th>
                                <th>job</th>
                                <th>Create at</th>
                                <th>Update at</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {data.map((value) => {
                                return (
                                    <tr>
                                        <td>{value.id}</td>
                                        <td>{value.name}</td>
                                        <td>{value.gender}</td>
                                        <td>{value.email}</td>
                                        <td>{value.job}</td>
                                        <td>{value.creAt}</td>
                                        <td>{value.upAt}</td>
                                        <td>
                                            <div>
                                                <Button variant="primary" data-bs-toggle="modal" data-bs-target="#exampleModal" onClick={() => render(<Detail data={data} />)}><BsEye/> View</Button>
                                                <Button className="m-2" variant="info"><BsArrowRepeat/> Update</Button>
                                                <Button variant="danger" onClick={() => deleteRow(index)}><BsTrash/> Delete</Button>
                                            </div>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <Row>
                        {data.map((data) =>
                            <Col md={3}>
                                <Cards data={data} deleteRow={deleteRow} />
                            </Col>
                        )}
                    </Row>
                </div>
            </div>
        </Container>
    )
}

export default TableData
